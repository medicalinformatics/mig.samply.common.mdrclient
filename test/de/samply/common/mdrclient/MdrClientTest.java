/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.common.mdrclient;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.google.common.base.Stopwatch;
import de.samply.common.mdrclient.domain.Catalogue;
import de.samply.common.mdrclient.domain.Code;
import de.samply.common.mdrclient.domain.DataElement;
import de.samply.common.mdrclient.domain.Definition;
import de.samply.common.mdrclient.domain.EnumElementType;
import de.samply.common.mdrclient.domain.EnumValidationType;
import de.samply.common.mdrclient.domain.Label;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.Slot;
import de.samply.common.mdrclient.domain.Validations;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An MDR client test.
 *
 * @author diogo
 *
 */
public class MdrClientTest {

  private final static Logger logger = LoggerFactory.getLogger(MdrClientTest.class);
  private final static String MDR_REST_URL = "https://mdr.miracum.org/rest/api/mdr/";
  private final static String NAMESPACE = "miracum1";
  private final static String DATAELEMENT_WITH_CATALOG_VALUEDOMAIN = "urn:miracum1:dataelement:1622:1";
  private final static String CATALOG = "urn:miracum1:catalog:8:1";
  private final static String CODE_FROM_DE_WITH_CATALOG = "urn:miracum1:code:162968:1";
  private final static String CODE_VALUE = "P80";
  private final static String DATAELEMENT_WITH_INTEGER_VALUEDOMAIN = "urn:miracum1:dataelement:1733:1";
  private final static String SEARCH_TERM_MULTIPLE_RESULTS = "leber";
  private final static String SEARCH_TERM_ONE_RESULT = "Sonstige Subarachnoidalblutung";
  // currently none in the miracum mdr
  private final static String RECORD_URN = "";

  /**
   * The language to use on the MDR calls.
   */
  private static final String MDR_LANGUAGE = "de";
  /**
   * MDR client initialisation for a test server.
   */
  private MdrClient mdrClient = new MdrClient(MDR_REST_URL);

  /**
   * Test a valid request.
   */
  @Test
  public final void testNamespaceMembersRequest() {
    String response = null;
    try {
      response = mdrClient.getJsonNamespaceMembers(MDR_LANGUAGE, null, null, NAMESPACE);
      logger.info(response);

      List<Result> elements = mdrClient.getNamespaceMembers(MDR_LANGUAGE, NAMESPACE);
      for (Result e : elements) {
        logger.info(e.getId() + " " + e.getDesignations().get(0).getDesignation());
      }
    } catch (Exception e) {
      fail("Connection failed.");
    }

    logger.info(response);
  }

  /**
   * Test a valid request.
   */
  @Test
  public final void testDataElementValidations() {
    // query for a data element type for validations
    try {
      String response = mdrClient
          .getJsonDataElementValidations(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN, MDR_LANGUAGE,
              null, null);
      logger.info(response.toString());

      Validations dataElementValidations = mdrClient
          .getDataElementValidations(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN,
              MDR_LANGUAGE);

      logger.info("validation type: " + dataElementValidations.getDatatype());
      logger.info("validation data: " + dataElementValidations.getValidationData());
      assertEquals(dataElementValidations.getValidationType(),
          EnumValidationType.INTEGER.toString());
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      fail("An exception occurred for a valid request.");
    }
  }

  /**
   * Test a valid request.
   */
  @Test
  public final void testCode() {
    // query for a data element type for validations
    try {
      String response = mdrClient.getJsonCode(CATALOG, CODE_FROM_DE_WITH_CATALOG, null, null);
      System.out.println(response);

      Code code = mdrClient.getCode(CATALOG, CODE_FROM_DE_WITH_CATALOG);

      System.out.println("value of the code: " + code.getCode());
      System.out.println("Urn of the code: " + code.getIdentification().getUrn());
      assertEquals(code.getCode(), CODE_VALUE);
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      fail("An exception occurred for a valid request.");
    }
  }

  /**
   * Test multiple valid request.
   */
  @Test
  public final void multipleMdrQueries() {
    String response;
    try {
      response = mdrClient.getJsonNamespaceMembers(MDR_LANGUAGE, null, null, NAMESPACE);
      logger.info(response);

      List<Result> elements = mdrClient.getNamespaceMembers(MDR_LANGUAGE, NAMESPACE);
      for (Result e : elements) {
        logger.info(e.getId() + " " + e.getDesignations().get(0).getDesignation());
      }

      // query for all elements in a root group
      List<Result> rootGroupMembers = mdrClient.getMembers(elements.get(0).getId(), MDR_LANGUAGE);
      for (Result r : rootGroupMembers) {
        logger.info(r.getId() + " " + r.getDesignations().get(0).getDesignation());
      }

      Result group = null;
      for (Result r : rootGroupMembers) {
        if (r.getType().compareTo(EnumElementType.DATAELEMENTGROUP.name()) == 0) {
          group = r;
        }
      }
      if (group != null) {
        logger.info("Getting data elements from group (belonging to root group)" + group.getId()
            + "...");
        List<Result> resultList = mdrClient.getMembers(group.getId(), MDR_LANGUAGE);
        for (Result r : resultList) {
          logger.info(r.getId() + " " + r.getDesignations().get(0).getDesignation());
        }
      } else {
        logger.warn("Could not find any elements from a group in a root group");
      }

      // query for the definition of a data element
      response = mdrClient.getJsonDataElementDefinition(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN,
          MDR_LANGUAGE, null, null);
      logger.info(response);

      Definition dataElementDefinition = mdrClient.getDataElementDefinition(
          DATAELEMENT_WITH_INTEGER_VALUEDOMAIN, MDR_LANGUAGE);
      logger.info(dataElementDefinition.getDesignations().get(0).getDesignation()
          + "- " + dataElementDefinition.getDesignations().get(0).getDefinition());

      assertNotNull(dataElementDefinition);

      if (!RECORD_URN.isEmpty()) {
        // query a record information
        logger.info("Querying a record...");
        List<Label> recordLabel = mdrClient
            .getRecordLabel("urn:mdr10:record:1:1", MDR_LANGUAGE);
        logger.info("Record label: " + recordLabel.get(0).getDefinition());
        List<Result> resultList = mdrClient
            .getRecordMembers("urn:mdr10:record:1:1", MDR_LANGUAGE);
        for (Result r : resultList) {
          logger.info("Record member: " + r.getId() + " "
              + r.getDesignations().get(0).getDesignation());
        }

        // search for text
        logger.info("Searching for some expression...");
        resultList = mdrClient.search("date", MDR_LANGUAGE, null);
        for (Result r : resultList) {
          logger.info(r.getId() + " " + r.getDesignations().get(0).getDesignation());
        }
      }

      // checking slots
      logger.info("Checking slots...");
      ArrayList<Slot> slots = mdrClient.getDataElementSlots(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN);
      for (Slot s : slots) {
        logger.info("Slot: " + s.getSlotName() + ", " + s.getSlotValue());
      }
      assertTrue(slots.isEmpty());
    } catch (ExecutionException | MdrConnectionException | MdrInvalidResponseException e1) {
      fail("An error occurred for valid MDR requests.");
    }
  }

  @Test
  public final void testGetDataElement() {
    try {
      final Stopwatch stopwatch = Stopwatch.createStarted();
      DataElement dataElement = mdrClient
          .getDataElement(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN, "en");
      logger.info(
          "First call execution time: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " "
              + TimeUnit.MILLISECONDS.name());
      stopwatch.reset().start();
      dataElement = mdrClient.getDataElement(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN, "en");
      logger.info(
          "Second call execution time: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " "
              + TimeUnit.MILLISECONDS.name());
      stopwatch.reset().start();
      dataElement = mdrClient.getDataElement(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN, "en");
      logger.info(
          "Third call execution time: " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " "
              + TimeUnit.MILLISECONDS.name());
      mdrClient.cleanCache();
      stopwatch.reset().start();
      dataElement = mdrClient.getDataElement(DATAELEMENT_WITH_INTEGER_VALUEDOMAIN, "en");
      logger.info("First call after cleaning cache execution time: " + stopwatch.stop()
          .elapsed(TimeUnit.MILLISECONDS) + " " + TimeUnit.MILLISECONDS.name());

      assertNotNull(dataElement);
      assertNotNull(dataElement.getDesignations());
      assertNotNull(dataElement.getIdentification());
      assertNotNull(dataElement.getSlots());
      assertNotNull(dataElement.getValidation());
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      fail("An Exception was caught: " + e.getMessage());
    }

  }

  /**
   * Test requesting parts of a catalogue.
   */
  @Test
  public final void testPartialCatalogue() {
    try {
      String response = mdrClient.getJsonDataElementCatalogue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN,
          CODE_FROM_DE_WITH_CATALOG, MDR_LANGUAGE,
          null, null);
      logger.info(response);

      Catalogue catalogue = mdrClient
          .getDataElementCatalogue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN, CODE_FROM_DE_WITH_CATALOG,
              MDR_LANGUAGE);

      assertNotNull(catalogue.getRoot());

      logger.info("Root element ID: " + catalogue.getRoot().getIdentification().getUrn());
      logger.info("Root element designation: "
          + catalogue.getRoot().getDesignations().get(0).getDesignation());
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      fail("An exception occurred for a valid request.");
    }
  }

  /**
   * Test getting code by value
   */
  @Test
  public final void testGetCodeByValue() {
    try {
      String response = mdrClient
          .getJsonDataElementCatalogueCodeByValue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN, CODE_VALUE,
              MDR_LANGUAGE,
              null, null);
      logger.info(response);

      Code code = mdrClient
          .getDataElementCatalogueCodeByValue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN, CODE_VALUE,
              MDR_LANGUAGE);

      assertNotNull(code);
      assertEquals(code.getCode(), CODE_VALUE);
      assertTrue(code.getSlots().size() > 0);
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      fail("An exception occurred for a valid request.");
    }
  }

  /**
   * Test requesting a filtered catalogue.
   */
  @Test
  public final void testFilteredCatalogue() {

    try {
      String response = mdrClient.getFilteredJsonDataElementCatalogue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN,
          MDR_LANGUAGE, SEARCH_TERM_MULTIPLE_RESULTS,
          null, null);
      logger.info(response);

      // Check for multiple elements
      Catalogue catalogue = mdrClient
          .getFilteredDataElementCatalogue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN, MDR_LANGUAGE,
              SEARCH_TERM_MULTIPLE_RESULTS);

      assertNotNull(catalogue.getRoot());
      assert(catalogue.getRoot().getSubCodes().size() > 1);

      logger.info("Root element ID: " + catalogue.getRoot().getIdentification().getUrn());
      logger.info("Root element designation: "
          + catalogue.getRoot().getDesignations().get(0).getDesignation());

      // Check for just one
      response = mdrClient.getFilteredJsonDataElementCatalogue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN,
          MDR_LANGUAGE, SEARCH_TERM_ONE_RESULT,
          null, null);
      logger.info(response);

      catalogue = mdrClient
          .getFilteredDataElementCatalogue(DATAELEMENT_WITH_CATALOG_VALUEDOMAIN, MDR_LANGUAGE,
              SEARCH_TERM_ONE_RESULT);

      assertNotNull(catalogue.getRoot());
      // When using the icd10 catalogue from the miracum mdr instance, all codes exist once more with a "+"
      // at the end. So actually we expect 2 results instead of one
      assert(catalogue.getRoot().getSubCodes().size() <= 2);
      if (catalogue.getRoot().getSubCodes().size() == 2) {
        String subCode1 = catalogue.getRoot().getSubCodes().get(0);
        String subCode2 = catalogue.getRoot().getSubCodes().get(1);
        if (subCode1.length() > subCode2.length()) {
          assert(subCode1.startsWith(subCode2));
        } else {
          assert(subCode2.startsWith(subCode1));
        }
      }

    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      fail("An exception occurred for a valid request.");
    }
  }

}
