/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.common.mdrclient;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import de.samply.common.mdrclient.MdrResponse.Type;
import de.samply.common.mdrclient.domain.DataElement;
import de.samply.common.mdrclient.domain.RecordDefinition;
import de.samply.common.mdrclient.domain.Result;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manager for the cache of the MDR.
 *
 * @author diogo
 */
public final class CacheManager {

  /**
   * Maximum size of the cache.
   */
  private static final int CACHE_MAXIMUM_SIZE = 10000;

  /**
   * The cache instance.
   */
  private static LoadingCache<CacheKey, String> jsonCache;

  /**
   * Language map to the caches.
   */
  public static ConcurrentHashMap<String, LanguageCache> objectCache = new ConcurrentHashMap<>();


  /**
   * The logger for this class.
   */
  private static Logger logger = LoggerFactory.getLogger(CacheManager.class.getName());

  /**
   * Prevent instantiation.
   */
  private CacheManager() {
  }

  /**
   * Static factory method to obtain a cache instance.
   *
   * @param mdrClient MDR Client instance, to be reused (with path and proxy settings already
   *                  loaded).
   * @return cache instance
   */
  public static LoadingCache<CacheKey, String> getCache(final MdrClient mdrClient) {
    if (jsonCache == null) {
      CacheLoader<CacheKey, String> loader = new CacheLoader<CacheKey, String>() {
        public String load(final CacheKey key) throws MdrConnectionException {
          logger.debug(
              "Element was not in cache: " + key.getPath() + " | " + key.getLanguageCode() + " | "
                  + key.getAccessToken());
          if (key.getAccessToken() != null) {
            return mdrClient
                .getJsonFromMdr(key.getPath(), key.getLanguageCode(), key.getAccessToken());
          } else {
            return mdrClient.getJsonFromMdr(key.getPath(), key.getLanguageCode());
          }
        }
      };
      jsonCache = CacheBuilder.newBuilder().maximumSize(CACHE_MAXIMUM_SIZE).build(loader);
    }

    return jsonCache;
  }

  /**
   * Get the languageCache for the given language.
   *
   * @param client   the mdrclient if no language cache exists.
   * @param language the target language.
   * @return the languageCache for the given language.
   */
  public static LanguageCache getLanguageCache(MdrClient client, String language) {
    if (!objectCache.containsKey(language)) {
      objectCache.put(language, new LanguageCache(client, language));
    }
    return objectCache.get(language);
  }

  /**
   * Get any languageCache. This is for case were language does not matter.
   *
   * @return a languageCache or null if non exists.
   */
  public static LanguageCache getLanguageCache() {
    for (Entry<String, LanguageCache> languageCacheEntry : objectCache.entrySet()) {
      return languageCacheEntry.getValue();
    }
    logger.error("A languageCache should exist a this stage");
    return null;
  }

  /**
   * PreCache the records or dataelements with this urn for the given languages.
   *
   * @param client      the mdrclient.
   * @param key         the elements urn.
   * @param languages   list of languages to be cached.
   * @param accessToken the accesstoken.
   * @throws ExecutionException exception while getting results.
   * @throws MdrConnectionException problem with mdr connection.
   * @throws MdrInvalidResponseException problem with mdrs answer.
   */
  public static void preCacheKey(MdrClient client, String key, List<String> languages,
      String accessToken)
      throws ExecutionException, MdrConnectionException, MdrInvalidResponseException {
    if (key.contains(":record:")) {
      CacheManager.preCacheRecordDefinition(client, key, languages, accessToken);
      CacheManager.preCacheRecordMembers(client, key, languages, accessToken);
    } else if (key.contains(":dataelement:")) {
      CacheManager.preCacheDataElement(client, key, languages, accessToken);
    }
  }

  /**
   * PreCache RecordMembers aka a lis of Result type objects (other mdr elements).
   *
   * @param client      the mdrclient.
   * @param key         the records urn.
   * @param languages   the languages that should be cached.
   * @param accessToken the accessToken if needed.
   */
  public static void preCacheRecordMembers(MdrClient client, String key, List<String> languages,
      String accessToken) {
    for (String currLang : languages) {
      preCacheRecordMembers(client, key, currLang, accessToken);
    }
  }

  /**
   * PreCache RecordMembers aka a lis of Result type objects (other mdr elements). Also precaches
   * the members and a RecordDefinition.
   *
   * @param client      the mdrclient.
   * @param key         the records urn.
   * @param language    the language that should be cached.
   * @param accessToken the accessToken if needed.
   */
  public static void preCacheRecordMembers(MdrClient client, String key, String language,
      String accessToken) {
    String path = "records/" + key + "/members";
    String response;
    try {
      response = client.getJsonFromMdr(path, language, accessToken);
      java.lang.reflect.Type listType = new TypeToken<ArrayList<Result>>() {
      }.getType();
      Gson gson = new Gson();
      List<Result> resultList = gson.<ArrayList<Result>>fromJson(response, listType);
      MdrResponse mdrResponse = new MdrResponse(key, path, language, accessToken, response,
          resultList, Type.RECORD_MEMBERS);
      CacheManager.getLanguageCache(client, language).putMdrResponse(mdrResponse);
      preCacheMembersInRecord(client, language, resultList, accessToken);
    } catch (MdrConnectionException e) {
      logger.warn("Unable to cache" + path, e);
    }
  }

  /**
   * Helper to also cache all members in the record.
   *
   * @param client        the mdrclient.
   * @param language      the language that should be cached.
   * @param recordMembers list of Results (mdr elements).
   * @param accessToken   the accessToken if needed.
   */
  private static void preCacheMembersInRecord(MdrClient client, String language,
      List<Result> recordMembers, String accessToken) {
    recordMembers.parallelStream().forEach(result -> {
      String currUrn = result.getIdentification().getUrn();
      try {
        CacheManager.preCacheKey(client, currUrn, Collections.singletonList(language), accessToken);
      } catch (ExecutionException | MdrConnectionException | MdrInvalidResponseException e) {
        logger.error("Unable to cache " + currUrn, e);
      }
    });
  }

  /**
   * Cache a RecordDefinition.
   *
   * @param client      the mdrclient.
   * @param key         the records urn.
   * @param languages   the languages that should be cached.
   * @param accessToken accessToken if needed.
   */
  public static void preCacheRecordDefinition(MdrClient client, String key, List<String> languages,
      String accessToken) {
    for (String language : languages) {
      preCacheRecordDefinition(client, key, language, accessToken);
    }
  }

  /**
   * Cache a RecordDefinition.
   *
   * @param client        the mdrclient.
   * @param key           the records urn.
   * @param language      the language that should be cached.
   * @param accessToken   accessToken if needed.
   */
  public static void preCacheRecordDefinition(MdrClient client, String key, String language,
      String accessToken) {
    String path = "records/" + key + "/labels";
    String response;
    RecordDefinition recordDefinition;
    try {
      response = client.getJsonFromMdr(path, language, accessToken);
      Gson gson = new Gson();
      recordDefinition = gson.fromJson(response, RecordDefinition.class);
      MdrResponse mdrResponse = new MdrResponse(key, path, language, accessToken, response,
          recordDefinition, Type.RECORD_DEFINITION);
      CacheManager.getLanguageCache(client, language).putMdrResponse(mdrResponse);
    } catch (MdrConnectionException e) {
      logger.warn("Unable to cache" + path, e);
    }
  }


  /**
   * Cache a DataElement.
   *
   * @param client      the mdrclient.
   * @param key         the dataElement urn.
   * @param languages   the languages that should be cached.
   * @param accessToken accessToken if needed.
   */
  public static void preCacheDataElement(MdrClient client, String key, List<String> languages,
      String accessToken) {
    for (String language : languages) {
      preCacheDataElement(client, key, language, accessToken);
    }
  }

  /**
   * Cache a DataElement.
   *
   * @param client      the mdrclient.
   * @param key         the dataElement urn.
   * @param language    the language that should be cached.
   * @param accessToken accessToken if needed.
   */
  public static void preCacheDataElement(MdrClient client, String key, String language,
      String accessToken) {
    String path = "dataelements/" + key;
    String response = null;
    DataElement dataElement;
    try {
      response = client.getJsonFromMdr(path, language, accessToken);
      Gson gson = new Gson();
      dataElement = gson.fromJson(response, DataElement.class);
      MdrResponse mdrResponse = new MdrResponse(key, path, language, accessToken, response,
          dataElement, Type.DATAELEMENT);
      CacheManager.getLanguageCache(client, language).putMdrResponse(mdrResponse);
    } catch (MdrConnectionException e) {
      logger.warn("Unable to cache" + path, e);
    }
  }

  /**
   * Cleans the cache of a specific MDR Client.
   *
   * @param mdrClient the MDR Client for which the cache will be cleaned.
   */
  public static void cleanCache(final MdrClient mdrClient) {
    getCache(mdrClient).invalidateAll();
    logger.debug("Cache cleaned.");
  }
}
