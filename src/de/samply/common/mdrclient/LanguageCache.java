/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.common.mdrclient;

import com.google.gson.Gson;
import de.samply.common.mdrclient.MdrResponse.Type;
import de.samply.common.mdrclient.domain.DataElement;
import de.samply.common.mdrclient.domain.RecordDefinition;
import de.samply.common.mdrclient.domain.Result;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LanguageCache {

  private MdrClient client;
  private String language;
  private ConcurrentMap<String, MdrResponse> responseMap = new ConcurrentHashMap<>();
  private int maxTryCount = 3;
  private static Logger logger = LoggerFactory.getLogger(LanguageCache.class.getName());

  public LanguageCache(MdrClient client, String language) {
    this.client = client;
    this.language = language;
  }

  /**
   * Put a MdrResponse in the cache.
   *
   * @param response a MdrResponse.
   */
  public void putMdrResponse(MdrResponse response) {
    String key = response.getType() + "_" + response.getKey();
    responseMap.put(key, response);
  }

  /**
   * Get a dataElement Object from the cache.
   *
   * @param urn targetUrn.
   * @return the dataElement.
   */
  public DataElement getDataElement(String urn) {
    String key = Type.DATAELEMENT + "_" + urn;
    int attempt = 0;
    while (!responseMap.containsKey(key) && attempt < maxTryCount) {
      attempt++;
      CacheManager.preCacheDataElement(client, urn, Collections.singletonList(language), null);
    }
    if (attempt == maxTryCount) {
      logger.error(
          "Unable to cache recordMembers for " + urn + " returning empty DataElement instead.");
      return new DataElement();
    }
    DataElement result = (DataElement) responseMap.get(key).getObject();
    return result;
  }

  /**
   * Get the recordMembers of a record.
   *
   * @param urn the records urn.
   * @return list of results.
   */
  public List<Result> getRecordMembers(String urn) {
    String key = Type.RECORD_MEMBERS + "_" + urn;
    int attempt = 0;
    while (!responseMap.containsKey(key) && attempt < maxTryCount) {
      attempt++;
      CacheManager.preCacheRecordMembers(client, urn, Collections.singletonList(language), null);
    }
    if (attempt == maxTryCount) {
      logger.error("Unable to cache recordMembers for " + urn + " returning empty list instead.");
      return new ArrayList<>();
    }
    List<Result> result = (List<Result>) responseMap.get(key).getObject();
    return result;
  }

  /**
   * Get the recordDefinition of a record.
   *
   * @param urn the records urn.
   * @return the recordDefinition.
   */
  public RecordDefinition getRecordDefinition(String urn) {
    String key = Type.RECORD_DEFINITION + "_" + urn;
    int attempt = 0;
    while (!responseMap.containsKey(key) && attempt < maxTryCount) {
      attempt++;
      CacheManager.preCacheRecordDefinition(client, urn, Collections.singletonList(language), null);
    }
    if (attempt == maxTryCount) {
      logger.error("Unable to cache recordMembers for " + urn
          + " returning empty RecordDefinition instead.");
      return new RecordDefinition();
    }
    RecordDefinition result = (RecordDefinition) responseMap.get(key).getObject();
    return result;
  }

  /**
   * Get the json representation of a DataElement.
   *
   * @param urn the elements urn.
   * @return the json string.
   */
  public String getDataElementJson(String urn) {
    String key = Type.DATAELEMENT + "_" + urn;
    if (!responseMap.containsKey(key)) {
      CacheManager.preCacheDataElement(client, urn, Collections.singletonList(language), null);
    }
    String result = responseMap.get(key).getResponse();
    return result;
  }

  /**
   * Get the json representation of a records members.
   *
   * @param urn the records urn.
   * @return the json string.
   */
  public String getRecordMembersJson(String urn) {
    String key = Type.RECORD_MEMBERS + "_" + urn;
    getRecordMembers(urn);
    String result = responseMap.get(key).getResponse();
    return result;
  }

  /**
   * Get the json representation of a dataelements validation.
   *
   * @param urn the elements urn.
   * @return the json string.
   */
  public String getDataElementValidationJson(String urn) {
    String key = Type.DATAELEMENT_VALIDATION + "_" + urn;
    if (!responseMap.containsKey(key)) {
      String dataElementKey = Type.DATAELEMENT + "_" + urn;
      DataElement dataElement = (DataElement) responseMap.get(dataElementKey).getObject();
      if (dataElement == null) {
        CacheManager.preCacheDataElement(client, urn, Collections.singletonList(language), null);
      }
      MdrResponse dataElementResponse = responseMap.get(dataElementKey);
      dataElement = (DataElement) dataElementResponse.getObject();
      Gson gson = new Gson();
      String validationString = gson.toJson(dataElement.getValidation());
      MdrResponse response = new MdrResponse(urn, dataElementResponse.getPath(),
          dataElementResponse.getLanguage(), dataElementResponse.getAccessToken(), validationString,
          dataElement.getValidation(), Type.DATAELEMENT_VALIDATION);
      putMdrResponse(response);
    }
    String result = responseMap.get(key).getResponse();
    return result;
  }

}
