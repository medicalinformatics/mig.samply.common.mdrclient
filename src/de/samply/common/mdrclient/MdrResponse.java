/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.common.mdrclient;

public class MdrResponse {

  enum Type {
    DATAELEMENT,
    DATAELEMENT_VALIDATION,
    RECORD_MEMBERS,
    RECORD_DEFINITION
  }

  private String key;
  private String path;
  private String language;
  private String accessToken;
  private String response;
  private Object object;
  private Type type;

  /**
   * Constructor.
   */
  public MdrResponse(String key, String path, String language, String accessToken, String response,
      Object object, Type type) {
    this.key = key;
    this.path = path;
    this.language = language;
    this.accessToken = accessToken;
    this.response = response;
    this.object = object;
    this.type = type;
  }

  /**
   * Get urn of the requested mdr element.
   * @return urn of the requested mdr element.
   */
  public String getKey() {
    return key;
  }

  /**
   * Get the path of the requested mdr element.
   * @return path of the requested mdr element.
   */
  public String getPath() {
    return path;
  }

  /**
   * Get the requested language.
   * @return requested language.
   */
  public String getLanguage() {
    return language;
  }

  /**
   * Get the used accessToken.
   * @return used accessToken.
   */
  public String getAccessToken() {
    return accessToken;
  }

  /**
   * Get the json response.
   * @return json response.
   */
  public String getResponse() {
    return response;
  }

  /**
   * Get the deserialized object.
   * @return deserialized object.
   */
  public Object getObject() {
    return object;
  }

  /**
   * Get type of object.
   * @return type of object.
   */
  public Type getType() {
    return type;
  }

}
