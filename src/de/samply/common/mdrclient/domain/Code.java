/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.common.mdrclient.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Catalogue code.
 *
 * @author diogo
 */
public class Code {

  /** Catalogue code URN. */
  private Identification identification;

  /** Designation of the catalogue code. */
  private List<Designation> designations = new ArrayList<>();

  /** Sub-codes of this data element. */
  private List<Object> subCodes = new ArrayList<>();

  /** Slots of this data element. */
  private List<Slot> slots = new ArrayList<>();

  /** Identifying code of the catalogue code. */
  private String code;

  /** Whether the code is valid (i.e. selectable) for the requested catalogue. */
  private Boolean isValid;

  /** Whether the code has child elements. */
  private Boolean hasSubcodes;

  public final Identification getIdentification() {
    return identification;
  }

  public final void setIdentification(final Identification identification) {
    this.identification = identification;
  }

  public final List<Designation> getDesignations() {
    return designations;
  }

  public final void setDesignations(final List<Designation> designations) {
    this.designations = designations;
  }

  public final List<Object> getSubCodes() {
    return subCodes;
  }

  public final void setSubCodes(final List<Object> subCodes) {
    this.subCodes = subCodes;
  }

  public List<Slot> getSlots() {
    return slots;
  }

  public void setSlots(List<Slot> slots) {
    this.slots = slots;
  }

  public final String getCode() {
    return code;
  }

  public final void setCode(final String code) {
    this.code = code;
  }

  public Boolean getValid() {
    return isValid;
  }

  public void setValid(Boolean valid) {
    isValid = valid;
  }

  public final Boolean getIsValid() {
    return isValid;
  }

  public final void setIsValid(final Boolean isValid) {
    this.isValid = isValid;
  }

  public final Boolean getHasSubcodes() {
    return hasSubcodes;
  }

  public final void setHasSubcodes(Boolean hasSubcodes) {
    this.hasSubcodes = hasSubcodes;
  }
}
