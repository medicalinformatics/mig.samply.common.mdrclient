# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.7.1] - 2022-07-07
### Fixed
- Record designations and definitions are being cached properly.

## [2.7.0] - 2021-05-17
### Added
- Deserialized Object Cache 

## [2.6.1] - 2020-05-26
### Fixed
- Added newline before package declaration

## [2.6.0] - 2020-05-26
### Added
- LICENSE.md and CONTRIBUTORS.md
### Changed
- apply google java code style
### Fixed
- copyright headers

## [2.5.0] - 2019-09-26
### Fixed
- proxy is correctly used

## [2.4.0] - 2019-08-02
### Added
- REST endpoint to retrieve filtered catalogues

## [2.3.1] - 2019-06-27
### Added
- Add slots to codes

## [2.3.0] - 2019-06-27
### Added
- Method to get a code by its code value instead of the urn

## [2.2.0] - 2019-06-17
### Added
- Methods to get only one level of descendants of catalogue nodes.

## [2.1.0] - 2019-06-14
### Changed
- Code reformatted to comply with google codestyle

## [2.0.0] - 2018-12-19
### Added
- CHANGELOG.md

### Changed
- Switched to jersey 2.x
